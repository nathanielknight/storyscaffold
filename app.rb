#!/usr/bin/env ruby

require 'sinatra'
require 'haml'



#User-facing routes
get '/' do
  haml :index
end

get '/random_next' do
  haml :random_next, layout: nil
end
