storyscaffold
=============

A blatant clone of story-dice on Sinatra.

If you've got ruby installed and can require sinatra, just run `app.rb`.
You can create a little story in your browser, with the app prompting you
to add the next sentence with (hopefully) interesting conjunctions.
